# Timing Files and Models

Each edat file from run is parsed to produce a directory containing timing files, named as `CBN01_AAA_0000_00_SE01_fMRI-R0_0001_task_timing_FSL`.  Within the directory are the following text files containing onsets, durations, and weights as expected for the FSL 3-column format.

## Timing Files

Fixations:
>fixation.csv

Faces -- congruent, incongruent (correct only):
>face_con.csv  
>face_inc.csv

Faces -- fearful, happy (correct only):
>face_fear.csv  
>face_hapy.csv

Faces -- conflict monitoring, all presented:
>CM_events_cC_unchk.csv  
>CM_events_cI_unchk.csv  
>CM_events_iC_unchk.csv  
>CM_events_iI_unchk.csv

Faces -- conflict monitoring, correct only:
>CM_events_cC_corr.csv  
>CM_events_cI_corr.csv  
>CM_events_iC_corr.csv  
>CM_events_iI_corr.csv

Faces -- emotional valence, all presented:
>EV_events_cFf_unchk.csv  
>EV_events_cFh_unchk.csv  
>EV_events_cHf_unchk.csv  
>EV_events_cHh_unchk.csv  
>EV_events_iFf_unchk.csv  
>EV_events_iFh_unchk.csv  
>EV_events_iHf_unchk.csv  
>EV_events_iHh_unchk.csv  

Faces -- emotional valence, correct only:
>EV_events_cFf_corr.csv  
>EV_events_cFh_corr.csv  
>EV_events_cHf_corr.csv  
>EV_events_cHh_corr.csv  
>EV_events_iFf_corr.csv  
>EV_events_iFh_corr.csv  
>EV_events_iHf_corr.csv  
>EV_events_iHh_corr.csv

Faces -- total errors:
>EV_events_err.csv

Faces -- errors of commission, duplication, omission:
>EV_events_err_com.csv  
>EV_events_err_dup.csv  
>EV_events_err_omi.csv

Faces -- post-error trials:
>EV_events_posterr.csv

Faces -- uncategorized trials (i.e. first face):
>EV_events_uncat.csv


## Models

The eight EV parameters have been incorporated into a model that also includes error, post-error, and uncategorized trials.  The exact timing differs by run, but a typical design matrix is shown with the associated contrasts in the following figure:

![Design Matrix](design.png)

And the covariance of the design is as follows:

![Design Covariance](design_cov.png)
